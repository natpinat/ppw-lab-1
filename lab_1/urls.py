from django.urls import path, include
from . import views
#url for app
urlpatterns = [
    path("", views.index, name='index'),
    path("about",views.about,name="about"),
    path("contact",views.contact, name ="contact")
]
